# Data Rider Roadmap

In this file, we define the overall roadmap of the Data Rider product for each stream.

## Stream Product

| Title                           | Description                                                                        | Business-value | Complexity | Target version | Release version |
|---------------------------------|------------------------------------------------------------------------------------|----------------|------------|----------------|-----------------|
| Easy local deployment on Docker | Remove build step and have only an easy deployment on Docker                       | +++            | ++         | 1.3            |                 |
| Easy local deployment on K8S    | Build a solution to deploy all the stack on K8S on the project computer            | +++            | +++        | 1.4            |                 |
| Global Documentation            | Build a website documentation to make the glue of all stream                       | +++            | +          | 1.3            |                 |
| Online subscription for player  | A web page, LinkedIn message or another online source to make an easy subscription | ++             | +++        | 1.4            |                 |
| Bilan carbone of a race         | Pouvoir mesurer le bilan carbone et le rapport environnemental de la course (ESG). | +++            | +++        | 1.4            |                 |
| Possibility to change AI mode   | Several AI mode + a way to select witch one we wan to use                          | ++             | +          | 1.3            |                 |


## Stream IOT (Internet of Things)

| Title                       | Description                                               | Business-value | Complexity | Target version | Release version |
|-----------------------------|-----------------------------------------------------------|----------------|------------|----------------|-----------------|
| Add more gate sensors       | To have more accuracy on Kart IA we need more gates       | +              | +++        | 2.x            |                 |
| Use current sensors         | Find a way to get current data without loosing some gates | ++             | +++        | 2.x            |                 |
| New hand for AI - DevFest   | Create a full hand for better impact on DevFest           | +++            | ++         | 1.2            | 1.2             |
| New hand for AI - All event | Create a full hand for better impact on all event         | +++            | ++         | 1.3            |                 |



## Stream ETL (Extract Transform Load)

| Title                              | Description                                                                          | Business-value | Complexity | Target version | Release version |
|------------------------------------|--------------------------------------------------------------------------------------|----------------|------------|----------------|-----------------|
| Reduce MTTS (Mean Time To Service) | Reduce time between data collect (IOT) and data exposition for website and dashboard | +++            | ++         | 1.3            |                 |



## Stream IA (Artificial Intelligence)

| Title                           | Description                                                                                      | Business-value | Complexity | Target version | Release version |
|---------------------------------|--------------------------------------------------------------------------------------------------|----------------|------------|----------------|-----------------|
| Generate custom player picture  | From a photo of the player, we generate a fun picture of a driver in the Data Rider theme        | +              | ++         | 1.4            |                 |
| Generate custom racing comments | Generate racing comments via generative AI based on the player's voice recording (~10s)          | -              | ++         | N/A            |                 |
| Detect sensor problem           | During building (to validate all sensor) or during a game, we detect a sensor with a malfunction | ++             | ++         | 1.4            |                 |
| MLOps                           | Build an MLOps pipeline on Cloud to upgrade Kart IA after each day                               | +++            | +++        | 2.x            |                 |


## Stream DataViz

| Title                 | Description                                                 | Business-value | Complexity | Target version | Release version |
|-----------------------|-------------------------------------------------------------|----------------|------------|----------------|-----------------|
| Have a nice dashboard | Upgrade current dashboard to be more visual and interesting | +++            | ++         | 1.4            |                 |

## Stream EPM (Enterprise Performance Management) 

| Title          | Description                                                                                                                                                                          | Business-value | Complexity | Target version | Release version |
|----------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------|------------|----------------|-----------------|
| KPI of project | Ajouter le prévisionnel sur les performances (nb de joueurs/nb de visiteurs). Faire des calculs d’écart entre les performances réelles et le prévisionnel projeté.                   |                |            |                |                 |
| Dashboard EPM  | Comparer les performances au fur et à mesure du salon. (Utilisateurs, intérêts, nb de participants, possibilité d’y ajouter des objectifs personnels (EPM) et des analyses d’écarts) |                |            |                |                 |
| Deployement    | Industrialiser le déploiement dans l’outil EPM et la cohérence entre le rôle Data et EPM                                                                                             |                |            |                |                 |



## Stream Digital/Web

| Title                   | Description                                                                                                                | Business-value | Complexity | Target version | Release version |
|-------------------------|----------------------------------------------------------------------------------------------------------------------------|----------------|------------|----------------|-----------------|
| Eco-design website      | Define a new style and refactor code to respect eco-design best practice                                                   | ++             | ++         | 1.4            |                 |
| Optimize UX             | Add new mode to play (define time for a game session, start counter and end counter, ...) + a better AI player integration | +++            | ++         | 1.3            |                 |
| LinkedIn UX player link | Find a way to connect a player from the LinkedIn profil (by a post, ...)                                                   | +++            | +++        | 1.4            |                 |

## Stream Digital/API

| Title                         | Description                                          | Business-value | Complexity | Target version | Release version |
|-------------------------------|------------------------------------------------------|----------------|------------|----------------|-----------------|
| API Between WebSite and BDD   | Build an API to expose Data to the website           | +              | ++         | 2.x            |                 |
| API Between Dashboard and BDD | Build an API to expose Data to the dashboard as code | +              | ++         | 2.x            |                 |  