#/bin/sh



#####################################################################
#####                 Init basics                               #####
#####################################################################
# Update the package list
sudo apt update

# Install curl (if not already available)
sudo apt-get install curl



#####################################################################
#####         Install Node.js and NPM for Hugo Template         #####
#####################################################################
# Install NVM
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/master/install.sh | bash

# Init for you current session
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" # This loads nvm bash_completion

# Test NVM is installed
nvm --version

# Install the current stable LTS release of Node.js
nvm install --lts

# Test nvm configuration
nvm ls

# Test Node.js is installed
node --version

# Test NPM is installed
npm --version


#####################################################################
#####                         Install HUGO                      #####
#####################################################################

# Install brew
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

# Update .bashrc to add brew available on terminal
(echo; echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"') >> eval echo "~$USER"/.bashrc
eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"

# Install HUGO with brew
brew install hugo

# Test hugo is installed
hugo version
