# CRON

Add a cron to aggregate data on clickhouse

## Open crontab UNIX
        crontab -e

## Edit with nano
Select editor for crontab, we choose 1 (nano)

## Add the cron
If the cron is not set, add this line

        * 6-21 * * MON-FRI /home/datarider/IdeaProjects/datarider-product/docker/cron.sh

## Analyze log of cron
    tail -f /var/log/cron