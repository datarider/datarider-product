# Prerequisites : run init-e2e.sh without error
# INIT
source .env

# ALL - Run services
echo "[INFO] Start services in docker"
docker compose up -d

# ETL - Scale Spark worker to 2
docker compose up -d --scale spark-worker=2

# VIZ - Add clickhouse connection
AUTH_TOKEN=$(http http://localhost:8088/api/v1/security/login username=admin password=admin provider=db | jq -r .access_token)
http http://localhost:8088/api/v1/database/ database_name="Datarider Clickhouse" engine=clickhouse sqlalchemy_uri=clickhousedb://clickhouse:8123/dr_gold Authorization:"Bearer ${AUTH_TOKEN}"

# ETL - Launch Spark Streaming applications
./run-spark.sh

# OPEN web pages
## OPEN Web UI
echo "[INFO] DataRider Web UI is opening in your default browser"
xdg-open $PUBLIC_HOME_URI

## OPEN Superset
echo "[INFO] Superset is opening in your default browser"
xdg-open "http://$LOCALHOST_URL:$SUPERSET_PORT"

# IOT - Expose sensor data to kafka
cd datarider-iot
source venv/bin/activate
python3 ./expose/serial2kafka.py



