## Clean all previous checkpoint
echo "[INFO] Clean Spark checkpoint"
docker compose exec -it spark /bin/bash -c "rm -R /tmp/tek/checkpoint_*"

## Execute Spark job
echo "[INFO] Launch Spark jobs"
nohup docker compose exec -it spark spark-submit --master spark://spark:7077 --class datarider.app.AggTrackSilverGoldStream --jars /datarider/datarider-assembly-0.0.3.jar /datarider/datarider_2.12-0.0.3.jar > logs/AggTrackSilverGoldStream.log &
