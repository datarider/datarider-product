# CLEAN PREVIOUS REPO
kia_project_name="datarider-ia-kart"
kia_dir="./${kia_project_name}"
sudo rm -d -R -f ${kia_dir}

# GET ALL REPO CONTENT
git clone --depth 1 --branch v1.1.2 https://gitlab.com/datarider/${kia_project_name}.git
sudo chmod 775 -R ${kia_dir}

### IOT - Create venv
cd ${kia_dir}
python3 -m venv --clear venv
source venv/bin/activate
pip install -r requirements.txt
deactivate
cd ..

