function apply_query() {
  echo "Execute query : '$1'"
  docker exec -it datarider-etl_clickhouse clickhouse-client --query "$1"
}

function export() {
  # _$(date +%s)
  apply_query "SELECT * FROM dr_gold.$1 INTO OUTFILE '/export/$1.csv' TRUNCATE FORMAT CustomSeparatedWithNames SETTINGS format_custom_field_delimiter = '|';"
}

export "dim_event"
export "dim_player"
export "dim_session"
export "dim_track"
export "etl_agg_track"
export "etl_all_session_player_v"
export "etl_all_session_turns_v"
export "etl_event_last_session_player_v"
export "etl_event_session_player_v"
export "etl_last_session_turns_v"
export "etl_turns_sections_v"
export "etl_turns_v"
export "fact_player_session"
