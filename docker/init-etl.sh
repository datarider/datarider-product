# CLEAN PREVIOUS REPO
sudo rm -d -R -f ./datarider-etl

# GET ALL REPO CONTENT
git clone --depth 1 https://gitlab.com/datarider/datarider-etl.git
sudo chmod 775 -R ./datarider-*

### ETL - Build package with assembly for Spark-Scala
docker compose stop spark
docker compose run sbt-package
sudo chmod 775 -R ./datarider-etl/target/

### ETL - Build and launch services
docker compose up -d spark --remove-orphans

### ETL - Check Spark JAR
NB_JAR=$(docker exec datarider-etl_spark /bin/bash -c 'ls -R /datarider/ | grep -c ".jar"')
if [ $NB_JAR -lt 1 ]
then
 echo "[ERROR] JAR for Spark still not found !"
 exit 1
fi
