# INIT for ETL
./init-etl.sh

# CLEAN PREVIOUS REPO
sudo rm -d -R -f ./datarider-viz
sudo rm -d -R -f ./datarider-iot
sudo rm -d -R -f ./datarider-wui

# GET ALL REPO CONTENT
git clone --depth 1 --branch v1.1.0 https://gitlab.com/datarider/datarider-viz.git
git clone --depth 1 --branch v1.1.0 https://gitlab.com/datarider/datarider-iot.git
git clone --depth 1 --branch v1.1.1 https://gitlab.com/datarider/datarider-wui.git
sudo chmod 775 -R ./datarider-*

### ALL - Add additionnal tools
sudo apt-get install jq
sudo apt install httpie

### ALL - Launch all services
docker compose up -d --remove-orphans

### WUI - Install Symfony dependencies
docker compose exec web composer install --no-interaction

### WUI - Create Postgres BDD
docker compose exec web php bin/console doctrine:database:create --no-interaction
docker compose exec web php bin/console doctrine:migrations:migrate --no-interaction

### WUI - Install node dependencies
docker compose run --rm node yarn install

### WUI - Build  assets (js, css et scss)
docker compose run --rm node yarn build

### GRAFANA - Install plugin
#docker exec -it datarider_grafana grafana cli plugins install grafana-clickhouse-datasource
#docker compose restart grafana

### IOT - Create venv
cd ./datarider-iot
python3 -m venv --clear venv
source venv/bin/activate
pip install -r requirements.txt
deactivate
cd ..
