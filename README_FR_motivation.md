## Motivation du projet

Pour des évènements ou des occasions tels que des salons (Salon de la Data, DevFest, … ),
l’entreprise dispose d’espaces dédiés (typiquements des stands) visant à accroitre 
sa visibilité auprès du publique et des acteurs professionnels. Ce sont des opportunités
pour exposer le savoir-faire de l’entreprise, présenter ses corps de métiers en ce qui concerne 
l’informatique appliqué au secteur de la data, recruter des talents et créer des contacts avec des
intervenants du secteur.

Néanmoins, un défi se présente lors de ces évènements :
rendre accessible et compréhensible à un maximum d’interlocuteur.ice.s
la complexité des activités de l’entreprise. Ces activités sont en effet très 
techniques, nombreuses, variées, et elles concernent tous les aspects de la chaîne de la donnée.

Le projet DataRider a pour ambition de venir répondre à cet 
objectif. Il possède plusieurs atouts qui sont les suivants :

•	Il propose tout d’abord **un environnement ludique et un visuel impactant**, à savoir 
un circuit électrique de voitures miniatures, où des participant.e.s peuvent venir jouer des
courses contre d’autres joueur.se.s ou bien contre une main mécanique pilotée par une IA. 
En terme de visuel, de nombreux éléments sont combinés : le circuit de jeu est en lui-même une attraction,
tout comme la main mécanique est un centre d’intérêt en elle-même. S’ajoutent à cela des éléments techniques 
visibles tels qu’un Arduino qui sert à collecter les données, et un Rasberry Pi qui sert à contrôler
la main mécanique, depuis un ordinateur. La partie électronique est ainsi mise à profit pour présenter 
une collecte de données physiquement et visuellement concrète.

•	Il permet **une implication du publique** puisque ce sont les propres données de 
jeu des joueur.se.s qui vont être collectées. Les personnes venant au stand sont engagées et 
actives de part le fait qu’elles jouent des parties de jeu. Dans un second temps, elles accèdent à 
leurs statistiques de jeu personnelles. Cela présente un intérêt fort : lors des explications visant 
à mettre en valeur les différents savoir-faire de l’entreprise, les procédés techniques à l’oeuvre 
pourront être présentés au travers de leurs applications aux données des joueur.se.s, et non à partir 
d’exemples génériques ou abstraits.

•	Il offre un environnement technique riche, complexe et varié qui est **apte 
à soutenir des possibilités de présentations exhaustives et approfondies des différents
secteurs des métiers de la data**, et apte à mettre en valeur ces métiers. Il est possible 
de citer 5 grandes parties : une **partie IoT**, qui regroupe le savoir-faire autour de l’électronique
et l’ingestion de données ; une **partie ETL**, qui valide les données, les enrichit et les rend disponibles
dans des bases de données, une **partie Web**, qui propose une interface utilisateur permettant aux joueurs 
de renseigner leur nom et leur mail, une **partie visualisation**, qui au travers 
d’un dashbord (business intelligence) donne les résultats de la course, et une **partie IA**, 
orientée sur le développement de modèles de machine learning.
