# Data Rider - Global product : Improve Mario Kart with IOT and AI

![datarider_blue.png](img/datarider_blue.png)

## Description

The project is composed by an electric car race track, where participants can play races against
other players or against a mechanical hand controlled by AI. Race data are collected, processed, 
leveraged and exposed.

One of our goals is to offer a code and organization of high quality, 
adhering to best practices in each of the technical domains, 
so that this code and organization can serve as a reference example for 
this type of technology. The technical domains present 
in the project's data pipeline are as follows:  
 - IoT (linked with the electronics part),, 
 - ETL,  
 - Web interface,   
 - Visualization (dashboard),  
 - IA. 

## Genesis of the project

For events or occasions such as trade shows (Salon de la Data, DevFest, etc.),
the company has dedicated spaces (typically booths) aimed at increasing
its visibility among the public and professional stakeholders. 
These are opportunities to showcase the company’s expertise, 
present its areas of work related to computing in the data sector, 
recruit talent, and create contacts with industry participants.

However, a challenge arises at these events: making the complexity 
of the company's activities accessible and understandable to a wide audience. 
These activities are indeed highly technical, numerous, varied, and cover all aspects of the data chain.
The DataRider project aims to address this objective. It has several advantages, which are as follows:

- Firstly, it offers an engaging environment and **an impactful visual**, 
namely a miniature car race track, where participants can play races against 
other players or against a mechanical hand controlled by AI. In terms of visuals, 
many elements are combined: the race track itself is an attraction, just as the mechanical 
hand is a center of interest in its own right. Additionally, visible technical elements 
such as an Arduino used for data collection, and a Raspberry Pi used to control 
the mechanical hand from a computer, are incorporated. The electronic components 
are thus utilized to present data collection in a physically and visually concrete manner.

- It allows for **public engagement** since it is the players' own game 
data that will be collected. Visitors to the booth are engaged and 
active as they play game sessions. Subsequently, they access their personal 
game statistics. This is of significant interest: 
during explanations aimed at highlighting the company’s various skills, 
the technical processes involved can be demonstrated through their application to 
the players' data, rather than through generic or abstract examples.

- It provides a rich, complex, and varied technical environment that **supports comprehensive 
and detailed presentations of the different sectors within the data field** and highlights 
these professions. The project is broken down into five main areas: an **IoT section**,
which encompasses expertise in electronics and data ingestion; an **ETL section**, which validates,
enriches, and makes data available in databases; a **Web section**, which offers 
a user interface allowing players to input their names and emails;
a **visualization section**, which provides race results through a 
dashboard (business intelligence); and an **AI section**, focused on
the development of machine learning models.


In the long term, the goal is to make it public. Thus, the project aims for 
a broader objective than just showcasing the company's expertise at events: the goal is to offer 
reusable and open-source code.

## Getting started
Read documentation **TODO : add link**

## Overview of the circuit and its dimensions

The car racing circuit is as follows. The numbers placed on the photo
indicate the location of each of the gates,
numbered from 1 to 8.

![Kart circuit](/img/track_sensor_position.png)


The length of the circuit is about **5 metres**. (**4.907 metres precisely**).

The lengths of tracks A (or track 1) and B (or 2) are the same (5 metres) when considering the circuit as a whole.

![Kart circuit](/img/track_sensor_position_with_tracks.png)

However, **when considering portions of the track (typically track sections between two gates)**, the distances covered can differ.
This is due to the curves:
the kart on the outer track covers a slightly longer distance than the one on the inner track.
Nevertheless, the circuit is designed so that overall,
both karts complete the same number of inner and outer turns: overall, they cover the same distance of about 5 meters (490.7 cm precisely).

The sections below detail the distances covered between each sensor on each of the two tracks A and B.

### Gate 1 to Gate 2

![12](/img/Gate1to2.PNG)

**L<sub>12</sub>** = 3 * short_straight_section + 1 * long_straight_section = **68.4 cm**

### Gate 2 to Gate 3

![23](/img/Gate2to3.PNG)

**Track A :**  
**L<sub>23_ext</sub>** = 1 * **outside_turn** + 2 * plate_sensor_offset = **38.3 cm**

**Track B :**  
**L<sub>23_int</sub>** = 1 * **inside_turn** + 2 * plate_sensor_offset = **29.4 cm**

**outside_turn** = 2π **r<sub>ext</sub>** / 4 = 31.3 cm  
**inside_turn** = 2π **r<sub>int</sub>** / 4 = 22.4 cm

### For others sections

The other sections were calculated using the same principle as the two previously presented.

### Lengths of track A

|                                      Section                                      |                            TOTAL                            | Long straight section (34.2cm) | Short straight section (11.4cm) | <span style="color:red; font-weight:bold;">Outside turn (31.3cm)</span> | <span style="color:green; font-weight:bold;">Inside turn (22.4cm)</span> | Looping (113cm) | Sensor offset (3.5cm) |
|:---------------------------------------------------------------------------------:|:-----------------------------------------------------------:|:------------------------------:|:-------------------------------:|:-----------------------------------------------------------------------:|:------------------------------------------------------------------------:|:---------------:|:---------------------:|
|                                      1 to 2                                       |                           68.4 cm                           |               1                |               3                 |                                  -                                      |                                    -                                     |        -        |           -           |
|             <span style="color:red; font-weight:bold;">2 to 3</span>              |     <span style="color:red; font-weight:bold;">38.3 cm</span> |               -                |               -                 |      <span style="color:red; font-weight:bold;">1</span>                |                                    -                                     |        -        |           2           |
|                                      3 to 4                                       |                           15.8 cm                           |               -                |               2                 |                                  -                                      |                                    -                                     |        -        |          -2           |
|            <span style="color:green; font-weight:bold;">4 to 5</span>             | <span style="color:green; font-weight:bold;">74.2 cm</span> |               -                |               -                 |                                  -                                      |         <span style="color:green; font-weight:bold;">3</span>            |        -        |           2           |
|                                      5 to 6                                       |                           38.6 cm                           |               1                |               1                 |                                  -                                      |                                    -                                     |        -        |          -2           |
|                                      6 to 7                                       |                           120 cm                            |               -                |               -                 |                                  -                                      |                                    -                                     |        1        |           2           |
|                                      7 to 8                                       |                           61.4 cm                           |               2                |               -                 |                                  -                                      |                                    -                                     |        -        |          -2           |
|             <span style="color:red; font-weight:bold;">8 to 1</span>              |   <span style="color:red; font-weight:bold;">74 cm</span>   |               -                |               1                 |      <span style="color:red; font-weight:bold;">2</span>                |                                    -                                     |        -        |           -           |
|                                       TOTAL                                       |                              -                              |               4                |               7                 |                                  3                                      |                                    3                                     |        1        |           0           |
|                                    TOTAL (cm)                                     |                          490.7 cm                           |           136.8 cm             |             79.8 cm             |                              93.9 cm                                    |                                 67.2 cm                                  |     113 cm      |         0 cm          |

### Lengths of track B

|                          Section                           |                            TOTAL                            | Long straight section (34.2cm) | Short straight section (11.4cm) | <span style="color:red; font-weight:bold;">Outside turn (31.3cm)</span> | <span style="color:green; font-weight:bold;">Inside turn (22.4cm)</span> | Looping (113cm) | Sensor offset (3.5cm) |
|:----------------------------------------------------------:|:-----------------------------------------------------------:|:------------------------------:|:-------------------------------:|:-----------------------------------------------------------------------:|:------------------------------------------------------------------------:|:---------------:|:---------------------:|
|                           1 to 2                           |                           68.4 cm                           |               1                |               3                 |                                  -                                      |                                    -                                     |        -        |           -           |
| <span style="color:green; font-weight:bold;">2 to 3</span> |    <span style="color:green; font-weight:bold;">29.4 cm</span> |               -                |               -                 |                                  -                                      |        <span style="color:green; font-weight:bold;">1</span>             |        -        |           2           |
|                           3 to 4                           |                           15.8 cm                           |               -                |               2                 |                                  -                                      |                                    -                                     |        -        |          -2           |
|  <span style="color:red; font-weight:bold;">4 to 5</span>  | <span style="color:red; font-weight:bold;">100.9 cm</span>  |               -                |               -                 |        <span style="color:red; font-weight:bold;">3</span>              |                                    -                                     |        -        |           2           |
|                           5 to 6                           |                           38.6 cm                           |               1                |               1                 |                                  -                                      |                                    -                                     |        -        |          -2           |
|                           6 to 7                           |                           120 cm                            |               -                |               -                 |                                  -                                      |                                    -                                     |        1        |           2           |
|                           7 to 8                           |                           61.4 cm                           |               2                |               -                 |                                  -                                      |                                    -                                     |        -        |          -2           |
| <span style="color:green; font-weight:bold;">8 to 1</span> | <span style="color:green; font-weight:bold;">56.2 cm</span> |               -                |               1                 |                                  -                                      |        <span style="color:green; font-weight:bold;">2</span>             |        -        |           -           |
|                           TOTAL                            |                              -                              |               4                |               7                 |                                  3                                      |                                    3                                     |        1        |           0           |
|                         TOTAL (cm)                         |                          490.7 cm                           |           136.8 cm             |             79.8 cm             |                              93.9 cm                                    |                                 67.2 cm                                  |     113 cm      |         0 cm          |



## Roadmap

### MVP : Validate the project idea (done)
- Kart track with several position sensors, current sensors and voltage sensors
- Arduino collected data
- GUI Mockups
- Dashboards Mockups

### V1 : Light end to end usage (in progress)
- First level of data preparation with Spark Streaming 
- Inscription and play with GUI 
- First level of monitoring for Kafka and Spark 
- First dashboards 
- IA driving a kart based on collected data
- First level of DevOps
- Online documentation

### V2 : Advanced end to end usage
- Advanced data preparation with Spark Streaming 
- Add user review on GUI 
- Automatic LinkedIn inscription by message (linked with GUI)
- Global monitoring
- Advanced dashboards 
- Personalized reporting of your kart session
- Advanced level of DevOps
- MLOps for IA driving

## Contributing
Orange Business and Business&Decision members only

## Authors and acknowledgment
List of contributors is defined in all README.md for each repository.

For this current repository, sorted by alphabetical name:
- LE PENMELEN Cédric as product owner & data architect
- NGUYEN Anh Vu as kubernetes developer
- PRESENTINI Olivier as devops & kubernetes expert

Special thanks to ROY Jean-Christophe for the Data Rider pictures.

## License
X11 License Distribution Modification Variant - See LICENSE file
