# Data Kart Racer - Env

## Description
Manage the local environnement with minikube

## Organisation
TODO

## Tools
Minikube


## Installation
1. With a Linux computer or Windows with WSL clone the project
2. Execute the script `env/launch-env.sh` to install all tools for the project
3. On **Grafana** `http://localhost:3000`
   - Connect with user `admin` and password `admin`
   - Go on **Connections** > **Data Sources** and then click on button **+ Add new data source**
   - Select `Prometheus`, leave default value except for **Prometheus server URL** you need to set : `http://prometheus-server:80`
   - Apply with button **Save & test**
   - Now, clic on the **+** button on the header, then **Import dashboard**
   - Go on **Import via grafana.com** and set value `6417` (see also `2115`). Then click on **Load** button.
4. 


# Notes temporaires

Pour se connecter à Postgresql depuis Grafana: 
`kubectl get services` et récupérer l'IP et le port de `postgres-datarider-srv`

Exemple: 10.107.208.207:5432

Datasets pour postgresql

INSERT INTO public.das_dat_kartposition_gold(event_ts, event_ms, position_id, track_num) VALUES 
('2023-09-19 16:00:00',0,1,1),
('2023-09-19 16:00:00',900000,2,1),
('2023-09-19 16:00:01',400000,3,1),
('2023-09-19 16:00:02',50000,4,1),
('2023-09-19 16:00:02',400000,5,1),
('2023-09-19 16:00:02',700000,6,1),
('2023-09-19 16:00:03',300000,7,1),
('2023-09-19 16:00:04',500000,8,1),
('2023-09-19 16:00:04',900000,1,1),
('2023-09-19 16:00:00',0,1,2),
('2023-09-19 16:00:00',950000,2,2),
('2023-09-19 16:00:01',460000,3,2),
('2023-09-19 16:00:02',250000,4,2),
('2023-09-19 16:00:02',650000,5,2),
('2023-09-19 16:00:02',800000,6,2),
('2023-09-19 16:00:03',300000,7,2),
('2023-09-19 16:00:04',490000,8,2),
('2023-09-19 16:00:04',850000,1,2),
('2023-09-19 16:00:04',850000,1,2);