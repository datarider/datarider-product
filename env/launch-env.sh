#!/bin/bash
##############################################################################################################
##### Main script to install (first time) and launch the global environnement of the project on minikube #####
##### Special thanks to Thomas R.                                                                        #####
##############################################################################################################

#
set -u
set -E
trap '[ "$?" -ne 255 ] || exit 255' ERR


##############################################################################################################
##### INIT var                                                                                           #####
##############################################################################################################
ENVPATH=$(dirname $0)
INSTALLDIR="install"
CONFIGDIR="config"
DEPLOYDIR="deploy"
ROOT_PATH=$(dirname $0)/../



##############################################################################################################
##### INSTALL on computer                                                                                #####
##############################################################################################################
echo "##### INSTALL on computer"

##### Install YQ
##### Lightweight and portable command-line YAML, JSON and XML processor
${ENVPATH}/${INSTALLDIR}/install-yq.sh


##### Install DOCKER 
##### Tool that is used to automate the deployment of applications in lightweight containers
${ENVPATH}/${INSTALLDIR}/install-docker.sh
DOCKER_IS_RUNNING=$(sudo service docker status | grep "Active: active (running)" | wc -l)
if [ "${DOCKER_IS_RUNNING}" != "1" ] 
then 
   echo -e "\033[0;31mDocker is not running\033[0m"
   echo -e "\033[0;31mTrying to start Docker\033[0m"
   sudo service docker start
fi


##### Install MINIKUBE
##### Tool that runs a single-node Kubernetes cluster in a virtual machine on your laptop
${ENVPATH}/${INSTALLDIR}/install-minikube.sh
MINIKUBE_IS_RUNING=$(minikube status | grep "apiserver: Running" | wc -l)
if [ "${MINIKUBE_IS_RUNING}" != "1" ] 
then 
   echo -e "\033[0;31mminikube is not running\033[0m"
   echo -e "\033[0;31mTrying to start minikube\033[0m"
   minikube start --cpus=4 --memory=4g --ports=443,80 --driver=docker
fi


##### Install HELM
##### Package manager for Kubernetes
${ENVPATH}/${INSTALLDIR}/install-helm.sh
CURRENT_HELM_VERSION=$(helm version | wc -l) 
if [ "${CURRENT_HELM_VERSION}" != "1" ] 
then
    echo -e "\033[0;31mHelm installation ended with problem\033[0m"
    helm version
fi


##### Install KUBECTL
##### Kubernetes command-line tool to deploy and manage applications on a Kubernetes cluster
${ENVPATH}/${INSTALLDIR}/install-kubectl.sh
CURRENT_KUBECTL_CONTEXT=$(kubectl config current-context)
echo CURRENT_KUBECTL_CONTEXT: ${CURRENT_KUBECTL_CONTEXT} 
if [ ${CURRENT_KUBECTL_CONTEXT} != "minikube" ] 
then
    echo -e "\033[0;31mChanging context to minikube\033[0m"
    kubectl config use-context minikube
fi



##############################################################################################################
##### DEPLOY on minikube                                                                                 #####
##############################################################################################################
echo "##### DEPLOY on minikube"

##### Deploy NAMESPACE datarider
## Create namespace
kubectl apply -f ${ENVPATH}/${DEPLOYDIR}/dpl-namespace.yaml
## Define as default namespace
kubectl config set-context $(kubectl config current-context) --namespace=datarider

##### Deploy Postgres
##### PostgreSQL is a powerful, open source object-relational database system that uses and extends the SQL language
kubectl apply -f ${ENVPATH}/${CONFIGDIR}/cm-postgres.yaml
kubectl apply -f ${ENVPATH}/${DEPLOYDIR}/dpl-postgres.yaml
PGPASSWORD=datarider_password
export PGPASSWORD


##### Deploy KAFKA
##### Distributed event store and stream-processing platform (based on Zookeeper)
## Create zookeeper
kubectl apply -f ${ENVPATH}/${DEPLOYDIR}/dpl-zookeeper.yaml
## Create kafka with service
kubectl apply -f ${ENVPATH}/${DEPLOYDIR}/dpl-kafka.yaml

## Deploy Kafka - Distributed event store and stream-processing platform (based on Zookeeper) with HELM
#helm install dr bitnami/kafka --set externalAccess.enabled=true,externalAccess.service.type=NodePort,externalAccess.autoDiscovery.enabled=true,serviceAccount.create=true,rbac.create=true,persistence.size=1Gi,externalAccess.service.domain=192.168.99.101



##### Deploy PROMETHEUS-OPERATOR AND PROMETHEUS
##### Get metrics on K8S from serveral services and then to display on Grafana
# https://blog.marcnuri.com/prometheus-grafana-setup-minikube
#helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
#helm upgrade --install prometheus prometheus-community/prometheus
#kubectl apply -f ${ENVPATH}/${DEPLOYDIR}/dpl-prometheusoperator.yaml
#kubectl apply -f ${ENVPATH}/${DEPLOYDIR}/dpl-prometheus.yaml

##### Deploy GRAFANA
##### Open source analytics & monitoring solution for every database.
kubectl apply -f ${ENVPATH}/${DEPLOYDIR}/dpl-grafana.yaml


##### Deploy SPARK-OPERATOR with helm
##### Operator to execute Spark Application on K8S
helm repo add spark-operator https://googlecloudplatform.github.io/spark-on-k8s-operator
#helm upgrade --install spark-operator spark-operator/spark-operator --namespace datarider --set sparkJobNamespace=datarider --set image.tag=v1beta2-1.3.3-3.1.1 --set commonLabels.project=datarider --set podLabels.project=datarider
helm upgrade --install spark-operator spark-operator/spark-operator --namespace datarider --values ${ENVPATH}/${DEPLOYDIR}/dpl-sparkoperator-values.yaml



##### Deploy APACHE SUPERSET
##### An open-source software application for data exploration and data visualization (at scale)
helm repo add superset https://apache.github.io/superset
helm upgrade --install --values ${ENVPATH}/${DEPLOYDIR}/dpl-superset-values.yaml superset superset/superset --namespace datarider

##### Deploy CLICKHOUSE
##### An open-source column-oriented DBMS for online analytical processing (OLAP) that allows users to generate analytical reports using SQL queries in real-time
helm install clickhouse oci://registry-1.docker.io/bitnamicharts/clickhouse --namespace datarider




##############################################################################################################
##### ADDITIONNAL                                                                                        #####
##############################################################################################################
echo "##### ADDITIONNAL"

##### Additionnal on POSTGRES
## Create main tables
kubectl exec -i postgres-0 -- psql -h 127.0.0.1 -U datarider --no-password -p 5432 postgres < ${ENVPATH}/scripts/postgres-create-table.sql
## Create referel tables and load data
kubectl exec -i postgres-0 -- psql -h 127.0.0.1 -U datarider --no-password -p 5432 postgres < ${ENVPATH}/scripts/postgres-load-ref.sql

##### Additionnal on KAFFA
## Create topic iot_dat_sensors_bronze (if not exist)
kubectl exec -it kafka-deployment-0 -- /bin/bash -c "kafka-topics --bootstrap-server localhost:9092 --create --if-not-exists --topic iot_dat_sensors_bronze --replication-factor 1 --partitions 3"
## Create topic iot_dat_sensors_silver (if not exist)
kubectl exec -it kafka-deployment-0 -- /bin/bash -c "kafka-topics --bootstrap-server localhost:9092 --create --if-not-exists --topic iot_dat_sensors_silver --replication-factor 1 --partitions 3"


###### EXPOSE services
## For Kafka
nohup kubectl port-forward service/kafka-service 9092:9092 &
## For Grafana
nohup kubectl port-forward service/grafana 3000:3000 &
## For Superset
nohup kubectl port-forward service/superset 8088:8088 &
## For Prometheus
#export POD_NAME=$(kubectl get pods --namespace datarider -l "app.kubernetes.io/name=prometheus,app.kubernetes.io/instance=prometheus" -o jsonpath="{.items[0].metadata.name}")
#nohup kubectl --namespace datarider port-forward $POD_NAME 9090 &
