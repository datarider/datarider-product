# Data Kart Racer - Env

## Description
Manage the local environnement with minikube

## Tools
Minikube, Helm

## Installation
1. With a Linux computer or Windows with WSL clone the project
2. Install Kubectl, Minikube and Helm (see env/launch-env.sh)
3. Execute the scrip `dpl-datarider.sh` to install all tools for the project. To clean up, execute `delete-datarider.sh` and then delete cluster Minikube
**Note:** Spark-application can fail because the the time downloading and installing Kafka is too long so spark application can't find kafka-bootstrap-server. To fix it, delete spark application and reapply its yaml
4. To inject some data to kafka topics, use this command :   
```
kubectl -n datarider run kafka-producer -ti --image=quay.io/strimzi/kafka:0.42.0-kafka-3.7.1 --rm=true --restart=Never -- bin/kafka-console-producer.sh --bootstrap-server kafka-deployment-kafka-bootstrap:9092 --topic iot-dat-sensor-bronze
```
5. To check the traffic in **Kafka** on **Grafana**, port-forward grafana pod :  
```
kubectl port-forward grafana-7b7bfd6ffc-llf88 3000:3000 -n datarider
```
And then go to `http://localhost:3030`
   - Connect with user `admin` and password `admin`
   - Go on **Connections** > **Data Sources** and then click on button **+ Add new data source**
   - Select `Prometheus`, leave default value except for **Prometheus server URL** you need to set : `http://prometheus-operated:9090`
   - Apply with button **Save & test**
   - Now, clic on the **+** button on the header, then **Import dashboard**
   - Go on **Import via grafana.com** and set value `6417` (see also `2115` or other dashboard in grafana-dashboard). Then click on **Load** button.

