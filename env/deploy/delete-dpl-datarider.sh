kubectl delete -f kafka-strimzi-deployment.yaml -n datarider
kubectl delete -f spark-application.yaml -n datarider
kubectl delete -f clickhouse-deployment.yaml -n datarider
kubectl delete -f prometheus-operator-deployment.yaml -n datarider
kubectl delete -f grafana.yaml -n datarider
helm uninstall superset -n datarider
