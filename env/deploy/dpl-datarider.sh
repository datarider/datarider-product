kubectl create namespace datarider # Namespace datarider
kubectl create -f 'https://strimzi.io/install/latest?namespace=datarider' -n datarider # CRD for Strimzi Kafka
kubectl create -f kafka-metrics-configmap.yaml -n datarider # config for JMX Exporter to collect metrics 
kubectl create -f kafka-topics.yaml -n datarider # Kafka Topics 
kubectl create -f kafka-strimzi-deployment.yaml -n datarider # Kafka Deployment
kubectl apply -f prometheus-additional.yaml -n datarider #Prometheus additional scrape config
kubectl create -f prometheus-rules.yaml -n datarider # Prometheus rules
kubectl create -f prometheus-operator-deployment.yaml -n datarider # Prometheus Deployment
kubectl create -f strimzi-pod-monitor.yaml -n datarider # Strimzi pod-monitor
kubectl create -f prometheus.yaml -n datarider # Prometheus
helm repo add spark-operator https://github.com/kubeflow/spark-operator # Repo spark-operator
helm install spark-operator spark-operator/spark-operator --values sparkoperator-values.yaml -n datarider # Spark-operator values
kubectl create -f spark-serviceaccount.yaml -n datarider # Spark service account
kubectl create -f spark-application.yaml -n datarider # Spark application
kubectl create -f grafana.yaml -n datarider # Grafana
#helm repo add bitnami https://charts.bitnami.com/bitnami 
#helm install postgresql bitnami/postgresql --values dpl-postgres-values.yaml -n datarider
kubectl create -f clickhouse-configmap.yaml -n datarider # Clickhouse config.xml, user.xml and dim_track.csv
kubectl create -f clickhouse-env-configmap.yaml -n datarider # Environment variable for Clickhouse
kubectl create -f clickhouse-initdb-configmap.yaml -n datarider # Script to init Clickhouse database
kubectl create -f clickhouse-pv.yaml -n datarider
kubectl create -f clickhouse-pvc.yaml -n datarider
kubectl create -f clickhouse-deployment.yaml -n datarider # Clickhouse deployment
kubectl create -f clickhouse-service.yaml -n datarider # Clickhouse service
helm repo add superset https://apache.github.io/superset
helm upgrade --install --values superset-values.yaml superset superset/superset -n datarider
#kubectl -n datarider run kafka-producer -ti --image=quay.io/strimzi/kafka:0.42.0-kafka-3.7.1 --rm=true --restart=Never -- bin/kafka-console-producer.sh --bootstrap-server kafka-deployment-kafka-bootstrap:9092 --topic iot-dat-sensor-bronze
#kubectl -n datarider run kafka-consumer -ti --image=quay.io/strimzi/kafka:0.42.0-kafka-3.7.1 --rm=true --restart=Never -- bin/kafka-console-consumer.sh --bootstrap-server kafka-deployment-kafka-bootstrap:9092 --topic das-dat-sensor-silver --from-beginning
