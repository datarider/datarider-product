-- DAS
CREATE TABLE IF NOT EXISTS public.das_dat_kartposition_gold(
    event_ts        TIMESTAMP       NOT NULL,
    event_ms        INTEGER         NOT NULL,
    position_id     INTEGER         NOT NULL,
    track_num       INTEGER         NOT NULL
);

CREATE TABLE IF NOT EXISTS public.das_dat_kartpower_gold(
    event_ts              	 TIMESTAMP    	  NOT NULL,
    event_ms_start        	 INTEGER      	  NOT NULL,
    event_ms_end          	 INTEGER      	  NOT NULL,
    track_num             	 INTEGER      	  NOT NULL,
    amp_min               	 DECIMAL(3,2) 	  NOT NULL,
    amp_avg               	 DECIMAL(3,2) 	  NOT NULL,
    amp_max               	 DECIMAL(3,2) 	  NOT NULL,
    volt_min              	 DECIMAL(3,2) 	  NOT NULL,
    volt_avg              	 DECIMAL(3,2) 	  NOT NULL,
    volt_max              	 DECIMAL(3,2) 	  NOT NULL,
    pos_start             	 INTEGER      	  NOT NULL,
    pos_startturn         	 INTEGER      	  NOT NULL,
    pos_endturn           	 INTEGER      	  NOT NULL,
    pos_startbigturn      	 INTEGER      	  NOT NULL,
    pos_endbigturn        	 INTEGER      	  NOT NULL,
    pos_startloop         	 INTEGER      	  NOT NULL,
    pos_endloop           	 INTEGER      	  NOT NULL,
    pos_lastturn          	 INTEGER      	  NOT NULL   
);


-- WUI
CREATE TABLE IF NOT EXISTS public.wui_play_rating_silver(
    id                      UUID                DEFAULT gen_random_uuid(),
    score                   SMALLINT            NOT NULL,
    tscreated               TIMESTAMP           DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS public.wui_play_session_silver(
    id                      UUID                DEFAULT gen_random_uuid(),
    name                    VARCHAR(50)         NOT NULL,
    ts_start                TIMESTAMP           NOT NULL,
    ts_end                  TIMESTAMP           NOT NULL,
    ts_created              TIMESTAMP           DEFAULT CURRENT_TIMESTAMP,
    is_active               BOOLEAN             DEFAULT false
);

CREATE TABLE IF NOT EXISTS public.wui_play_player_silver(
    id                      UUID                DEFAULT gen_random_uuid(),
    sess_id                 INTEGER             NOT NULL,
    name                    VARCHAR(50)         NOT NULL,
    email                   VARCHAR(200)        NOT NULL,
    kart_id                 SMALLINT            NOT NULL,
    track_num               SMALLINT            NOT NULL,
    ts_created              TIMESTAMP           DEFAULT CURRENT_TIMESTAMP
);

