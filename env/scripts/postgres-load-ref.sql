-- iot_ref_sensortype_bronze
DROP TABLE IF EXISTS public.iot_ref_sensors_bronze;
CREATE TABLE public.iot_ref_sensors_bronze(
    pin                    VARCHAR(5)                  NOT NULL,
    sensor_type            VARCHAR(5)                  NOT NULL,
    position_id            SMALLINT                    NOT NULL,
    track_num              SMALLINT                    NOT NULL,
    tek_ts_created         TIMESTAMP WITH TIME ZONE    DEFAULT CURRENT_TIMESTAMP
);
INSERT INTO public.iot_ref_sensors_bronze (pin, sensor_type, position_id, track_num) VALUES
('A0',	'AMP',	'1',	'2'),
('A1',	'AMP',	'1',	'1'),
('A2',	'VOLT',	'0',	'1'),
('A3',	'VOLT',	'0',	'2'),
('22',	'HALL',	'1',	'1'),
('23',	'HALL',	'2',	'1'),
('24',	'HALL',	'3',	'1'),
('25',	'HALL',	'4',	'1'),
('26',	'HALL',	'5',	'1'),
('27',	'HALL',	'6',	'1'),
('28',	'HALL',	'7',	'1'),
('29',	'HALL',	'8',	'1'),
('30',	'HALL',	'1',	'2'),
('31',	'HALL',	'2',	'2'),
('32',	'HALL',	'3',	'2'),
('33',	'HALL',	'4',	'2'),
('34',	'HALL',	'5',	'2'),
('35',	'HALL',	'6',	'2'),
('36',	'HALL',	'7',	'2'),
('37',	'HALL',	'8',	'2');


--- iot_ref_sensortype_bronze
DROP TABLE IF EXISTS public.iot_ref_sensortype_bronze;
CREATE TABLE public.iot_ref_sensortype_bronze(
    sensor_type            VARCHAR(5)                   NOT NULL,
    value_type             VARCHAR(15)                  NOT NULL,
    value_desc             VARCHAR(50)                  NOT NULL,
    value_unit             VARCHAR(10)                  NOT NULL,
    tek_ts_created         TIMESTAMP WITH TIME ZONE     DEFAULT CURRENT_TIMESTAMP
);
INSERT INTO public.iot_ref_sensortype_bronze (sensor_type, value_type, value_desc, value_unit) VALUES
('AMP',	    'FLOAT',	'Electrical current',	    'mA'),
('VOLT',	'FLOAT',	'Electrical voltage',	    'V'),
('HALL',	'TINYINT',	'1 = sleep, 0 = activate',	'Bool');


-- iot_ref_trackposition_bronze
DROP TABLE IF EXISTS public.iot_ref_trackposition_bronze;
CREATE TABLE public.iot_ref_trackposition_bronze(
    id                     SMALLINT                    NOT NULL,
    description            VARCHAR(50)                 NOT NULL,
    next_position          SMALLINT                    NOT NULL,
    distance               INT                         NOT NULL,
    tek_ts_created         TIMESTAMP WITH TIME ZONE    DEFAULT CURRENT_TIMESTAMP
);
INSERT INTO public.iot_ref_trackposition_bronze (id, description, next_position, distance) VALUES
('0',	'No specific location',	                    '0',	'0'),
('1',	'Start zone',	                            '2',	'150'),
('2',	'End of straight / Start of first turn',	'3',	'50'),
('3',	'End of first turn',	                    '4',	'150'),
('4',	'Start of big turn',	                    '5',	'150'),
('5',	'End of big turn',	                        '6',	'300'),
('6',	'Start looping',	                        '7',	'150'),
('7',	'End looping',	                            '8',	'200'),
('8',	'Start of last turn',	                    '1',	'50');


-- das_ref_sensors_silver
DROP TABLE IF EXISTS public.das_ref_sensors_silver;
CREATE TABLE public.das_ref_sensors_silver(
    pin                     VARCHAR(5)                  NOT NULL,
    sensor_type             VARCHAR(5)                  NOT NULL,
    position_id             INTEGER                     NOT NULL,
    track_num               INTEGER,
    value_type              VARCHAR(15)                 NOT NULL,
    value_unit              VARCHAR(10)                 NOT NULL,
    tek_ts_created          TIMESTAMP WITH TIME ZONE    DEFAULT CURRENT_TIMESTAMP
);
INSERT INTO public.das_ref_sensors_silver (pin, sensor_type, position_id, track_num, value_type, value_unit)
SELECT
    sensors.pin,
    sensors.sensor_type,
    sensors.position_id,
    sensors.track_num,
    sensortype.value_type,
    sensortype.value_unit
FROM public.iot_ref_sensors_bronze sensors
LEFT JOIN public.iot_ref_sensortype_bronze sensortype
    ON sensors.sensor_type = sensortype.sensor_type;

