#!/bin/bash
# Docker install depends on Linux distribution
# Special thanks to Thomas R.

# Define function installation depends on Linux distribution
Install_On_RedHat()
{
    if [ $(cat /proc/version | grep WSL) == "" ]
    then
        sudo yum install -y yum-utils
        sudo yum-config-manager --add-repo https://download.docker.com/linux/rhel/docker-ce.repo
        sudo yum install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
        sudo systemctl start docker
        sudo usermod -aG docker $(whoami) 
    else
        echo -e "\033[0;31mRunning on wsl, RedHat distribution not supported\033[0m"
        Install_Not_Supported
    fi
}

Install_On_Debian()
{
    
    sudo apt-get update
    sudo apt-get install -y ca-certificates curl gnupg lsb-release
    sudo mkdir -m=755 /etc/apt/keyrings
    . /etc/os-release
    echo ${ID}
    curl -fsSL https://download.docker.com/linux/${ID}/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
    echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/${ID} \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    sudo apt-get update
    sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin
    sudo update-alternatives --set iptables /usr/sbin/iptables-legacy
    sudo update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy
    sudo service docker start
    sudo usermod -aG docker $(whoami) 
}

Install_On_Alpine()
{
    sudo apk add --update docker openrc
    service docker start
    sudo addgroup $(whoami) docker
    sudo rc-update add cgroups
}

Install_Not_Supported()
{
   echo -e "\033[0;31mDistribution trop exotique, merci d'utiliser une version commune\033[0m"
}

Install_On_Others()
{
   echo -e "\033[0;31mDistribution trop exotique, merci d'utiliser une version commune\033[0m"
}

# Get the Linux distribution of the current system
declare -A osInfo;
osInfo[/etc/redhat-release]="Install_On_RedHat"
osInfo[/etc/arch-release]="Install_On_Others"
osInfo[/etc/gentoo-release]="Install_On_Others"
osInfo[/etc/SuSE-release]="Install_On_Others"
osInfo[/etc/debian_version]="Install_On_Debian"
osInfo[/etc/alpine-release]="Install_On_Alpine"

# Execute installation
if [ "$(which docker)" == "" ]
    then
        echo -e "\033[0;31mInstallation de docker\033[0m"
        read -r -p "Are you sure? [y/N] " response
        case "$response" in
            [yY][eE][sS]|[yY]) 
                for f in ${!osInfo[@]}
                do
                    if [[ -f $f ]];then
                        ${osInfo[$f]}
                        echo -e "\033[0;31mMerci de taper la commande suivante : newgrp docker\033[0m"
                        echo -e "\033[0;31mPuis relancer le script\033[0m"
                        exit -1;
                    fi
                done
                ;;
            *)
                echo -e "\033[0;31mthis script depends on docker\033[0m"
                exit -1;
                ;;
        esac
    fi