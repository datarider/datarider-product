#!/bin/bash

if [ "$(which helm)" == "" ]
then
    echo -e "\033[0;31mInstallation de helm\033[0m"
    read -r -p "Are you sure? [y/N] " response
    case "$response" in
        [yY][eE][sS]|[yY]) 
            curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
            chmod 700 get_helm.sh
            ./get_helm.sh
            sudo install -o root -g root -m 0755 helm /usr/local/bin/helm
            rm get_helm.sh
            ;;
        *)
            echo -e "\033[0;31mthis script depends on help\033[0m"
            exit -1;
            ;;
    esac
fi