#!/bin/bash

if [ "$(which yq)" == "" ]
then
    echo -e "\033[0;31mInstallation de yq\033[0m"
    read -r -p "Are you sure? [y/N] " response
    case "$response" in
        [yY][eE][sS]|[yY]) 
            sudo wget -qO /usr/local/bin/yq https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64
            sudo chmod a+x /usr/local/bin/yq
            ;;
        *)
            echo -e "\033[0;31mthis script depends on yq\033[0m"
            exit -1;
            ;;
    esac
fi