#!/bin/bash

if [ "$(which minikube)" == "" ]
then
    echo -e "\033[0;31mInstallation de minikube\033[0m"
    read -r -p "Are you sure? [y/N] " response
    case "$response" in
        [yY][eE][sS]|[yY]) 
            curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
            sudo install minikube-linux-amd64 /usr/local/bin/minikube
            minikube start --cpus=4 --memory=8g --ports=443,80 --driver=docker
            ;;
        *)
            echo -e "\033[0;31mthis script depends on minikube\033[0m"
            exit -1;
            ;;
    esac
fi