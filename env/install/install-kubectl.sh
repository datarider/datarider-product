#!/bin/bash

if [ "$(which kubectl)" == "" ]
then
    echo -e "\033[0;31mInstallation de kubectl\033[0m"
    read -r -p "Are you sure? [y/N] " response
    case "$response" in
        [yY][eE][sS]|[yY]) 
            curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
            sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
            ;;
        *)
            echo -e "\033[0;31mthis script depends on kubectl\033[0m"
            exit -1;
            ;;
    esac
fi