#!/bin/bash
##############################################################################################################
##### Main script to stop minikube serivces                                                              #####
##############################################################################################################

# Delete all ressources (keep data)
kubectl delete all -l project=datarider --namespace datarider
kubectl delete all -l app.kubernetes.io/instance=spark-operator --namespace datarider
kubectl delete all -l app.kubernetes.io/instance=superset --namespace datarider
