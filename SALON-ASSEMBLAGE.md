# Data Rider - Salon - Notice d'assemblage

Ce document est en français uniquement pour le moment afin d'avoir déjà une première version fonctionnelle.

Si vous voyez qu'il manque des parties importantes, merci de nous le signaler pour améliorer cette notice?

## Check-list du matériel à emmener (pour salon ou autre évènement)

### Matériel obligatoire minimal
- [ ] Table du circuit
- [ ] Circuit (tous les rails et les fixations associées)
- [ ] Bordures (papier plastifié) de sécurité et ses connecteurs blancs : pour éviter que la voiture n'aille trop loin en cas de sortie
- [ ] 1 alimentation circuit
- [ ] 1 câble USB pour relier l’Arduino au PC (cable long dans l'idéal)
- [ ] 1 alimentation Arduino
- [ ] 2 manettes (M1 et M2)
- [ ] 4 voitures Mario Kart / Luigi (avec aimant)
- [ ] PC portable du projet
- [ ] Câble d'alimentation adaptée au PC projet
- [ ] 16 capteurs effet hall
- [ ] 1 multiprise 5P+T
- [ ] 1 tapis : pour protéger le circuit pendant le transport
- [ ] 4 sangles de fixation : pour tenir les rails et le tapis autour de table pliée lors du transport

![Circuit table rails](/img/assembly/circuit_disassembly.jpg)
![[Matériel obligatoire]](/img/assembly/material_mandatory.jpg)

### Matériel obligatoire pour main/IA
- [ ] 1 alimentation RaspPy (selon la version du RaspPy utilisé)
- [ ] 1 manette adaptée pour main IA (sans le ressort et avec un aimant)
- [ ] 1 main pour pilotage par IA
- [ ] 1 cable USB serial USB : pour piloter le Raspberry depuis le PC

![[Matériel obligatoire main IA]](/img/assembly/material_mandatory_hand_ia.jpg)

### Matériel très recommandé (pour réparation / changement)
- [ ] 1 cable HDMI : pour afficher les dashboard sur un écran annexe
- [ ] Clavier et souris (pour se brancher sur la RaspPy) : en autoboot sur le script, mais au moindre soucis ça peut être compliqué sans
- [ ] Un bloc de rail de remplacement par type de rail
  - [ ] 1 droit long
  - [ ] 1 droit court
  - [ ] 1 courbe
  - [ ] 1 looping
  - [ ] 1 plaque de démarrage (branchement alim et manette)
- [ ] Jupe blanche de table : beaucoup plus propre, surtout pour stocker le matériel dessous
- [ ] 2 manettes de rechanges
- [ ] 2 voitures Mario Kart / Luigi (avec aimant) supplémentaire : ça casse facilement
- [ ] 2 supports rouge : pour faire tenir les rails en hauteur
  - [ ] 1 haut
  - [ ] 1 bas
- [ ] Boitier tournevis
- [ ] Boitier avec vis
- [ ] Raspberry Pi de secours (prêt à l'usage : script Py installé, autoboot, hat mis en place)
- [ ] Arduino de secours (prêt à l'usage : script de collecte des données)
- [ ] 2 à 4 servomoteurs : pour remplacement sur une main
- [ ] 1 manette de secours adaptée pour main IA (sans le ressort et avec un aimant)
- [ ] des aimants pour mettre sur les roues des voitures
- [ ] de la colle extra forte
- [ ] au moins 4 capteurs à effet hall de rechange
- [ ] 1 seconde multiprise
- [ ] 1 rallonge

![[Matériel recommandé fortement]](/img/assembly/material_recommended_lvl1.jpg)

### Matériel recommandé
- [ ] 1 main de rechange (déjà bien calibrée)
- [ ] Écran secondaire : à emmener ou prévoir sur place - pour voir l'IHM du RaspPy et/ou faire défiler des slides en mode vidéo
- [ ] Kakemono / cadres pour inciter les visiteurs à jouer
- [ ] 1 hub avec sortie HDMI (x2 idéalement)
- [ ] 1 cable display port (à utiliser avec le hub)
- [ ] 1 multimètre
- [ ] 1 cable ethernet : pour connecter PC portable au raspberry en SSH
- [ ] 1 cable USB serial USB de secours

![[Matériel recommandé]](/img/assembly/material_recommended_lvl2.jpg)

## Etapes de démontage
1. Débrancher les fils sous le circuit reliant la main pour l'IA
2. Ranger les voitures, la main et les manettes pour libérer le circuit
3. Décrocher les rails empêchant la fermeture de la table ou dépassant une fois pliée pour correspondre à l'image ci-dessous

![Circuit démontage](/img/assembly/circuit_disassembly.jpg)

4. Ranger les plots rouges et le connecteur du looping
5. Ranger les rails non attachés (le rail du capteur 7 aussi car il ne tient pas sinon)
6. Sangler chaque côté de la table (voir image ci-dessous)

![Circuit démontage_attache](/img/assembly/circuit_disass-fixed.jpg)

7. Faire basculer la table sur le côté (attention aux scratch autour de la table qui s'accrochent à la moquette)
8. Plier les pieds (enlever sécurité) et les clipser au support (1 par côté)
9. Plier la table en 2 pour la refermer (débloquer la sécurité jaune au niveau de la jonction sous la table==
== rajouter photo==
10. Mettre le tapis noir sous la table et faire remonter sur les bords (voir photo)
== rajouter photo==
11. Sangler l'ensemble (table + circuit + tapis de protection)
== rajouter photo==
12. Ranger tout le matériel nécessaire dans la valise dédiée au projet (voir check-list)

## Etapes de montage du circuit (avec ou sans la main IA)
1. Retirer les sangles et le tapis mousse protégeant la table
2. Déplier la table (ouvrir et bloquer avec les sécurités pour ne pas qu'elle se referme)
3. Mettre la table debout toujours avec les sangles

![Circuit démontage_attache](/img/assembly/circuit_disass-fixed.jpg)

4. Enlever les sangles tenant le circuit
5. Remonter l'ensemble du circuit (rajouter les rails rangés et les poteaux rouges de fixation des rails en hauteur, et le plaque de jonction pour le looping)
6. Brancher les 2 manettes et l'alimentation du circuit
7. Positionner 2 voitures sur le circuit (1 par voie)
8. Tester le bon fonctionnement du circuit (et sa stabilité) pour chaque voiture
9. Brancher le PC du projet et l'allumer
11. Brancher l'alimentation de l'Arduino 
12. Ajouter les 16 capteurs effet hall
13. Tester manuellement chaque capteur avec un kart (en passant l'aimant de la roue à proximité) pour s'assurer que chaque capteur est bien branché et fonctionnel (il doit s'allumer quand l'aimant est devant)
14. Brancher le cable USB sur l'arduino et le relier au PC du projet
15. Aller dans le dossier `docker` et lancer le script `./run-e2e.sh`
16. Aller sur l'interface `WUI - Admin` pour créer l'évènement du salon (si ce n'est pas déjà fait)
17. Aller sur l'interface `WUI` classique pour créer une session de jeu
18. Aller sur l'interface de `Superset` et ouvrir le dashboard `LIVE`  pour visualiser la session de jeu en cours
19. Tester le circuit avec chaque voiture et vérifier la bonne remontée des données dans le dashboard LIVE 
20. Ajouter la jupe blanche pour cacher le dessous du circuit

## Compléments d'étapes de montage pour la main IA
1. Positionner la main IA sur son socle et passer les 2 fils du connecteur dans le trou 
2. Brancher les 2 connecteurs à ceux sous la table. Il est écrit IX (pour index) et PO (pour le pouce) sur les 2 connecteurs pour ne pas se tromper (même côté d'écriture)
3. Brancher l'alimentation du Raspberry
4. Brancher la manette IA de la main sur une des voies (INT = intérieur, EXT = extérieur)
4. Aller dans le dossier `docker` et lancer le script `./run-kia.sh`. 
5. Choisir voie 1 (=extérieur) ou voir 2 (=intérieur) quand le script le demande
6. Choisir le mode 1 (=slow) pour tester à vitesse réduite l'IA
5. Mettre une voiture sur la voie pilotée par l'IA pour valider le bon fonctionnement

### Illustration de la cible

![Circuit Illustration Finale](/img/circuit-illustration-final.jpg)

## Connections

### Etapes de montage
- Vérifier que le bloc orange sous le rail est bien fixé et que le connecteur noir correspond à l'emplacement prévu (voir illustration cible)
- Brancher les 16 capteurs à effet hall sur les emplacements prévus 
- Brancher l'alimentation du Raspberry
- Les 16 capteurs doivent être éteint par défaut (si un capteur est allumé, voir Problèmes rencontrés)
- En passant l'aimant d'une voiture devant chaque capteur, celui-ci doit s'allumer si il est bien branché

### Illustration de la cible
Chaque connecteur de capteur doit être à sa place, ils sont numérotés selon le pattern suivant : `G<gate_number>-<E/I>`
Avec :
- `<gate_number>` correspondant au numéro de la porte (voir schéma ci-dessous + indiqué également sur le rail)
- `<E/I>` correspondant à la voie **E**xtérieur ou **I**ntérieur
Exemples: `G1-E`, `G1-I`, `G7-I`

![Emplacement des portes](/img/track_sensor_position.png)

## Functional tests

### Fonctionnement du circuit
1. Vérifier que le circuit est complètement monté
2. Vérifier que l'alimentation du circuit est branchée
2. Vérifier que les 2 manettes joueurs sont branchées
3. Positionner une voiture sur chaque rail du circuit
4. Faire un tour complet avec chaque voiture

### Fonctionnement des capteurs


### Fonctionnement de la main


### Fonctionnement de l'IA


## Problèmes rencontrés
Cette section explique ce qui peut être fait en cas de problème rencontré durant la procédure.

### Capteur effet hall (porte) qui reste allumé
Il y a surement une inversion du connecteur noir (dans le bloc orange sous le rail). 
Il faut alors démonter le bloc, et changer le sens du connecteur noir.
Ce problème ne doit pas arriver si le circuit a été bien démonté et que tous les connecteurs sont restés dans leur bloc orange.

